# SRA-extract-experiment-ID.py : associate an SRA run ID with its sample
#                                ID and project ID
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

import sys
import time
import re
import urllib

SRRPattern = re.compile("SRR[0-9]*")
SRXPattern = re.compile("SRX[0-9]*")
SRSPattern = re.compile("SRS[0-9]*")
sraURL = "https://www.ncbi.nlm.nih.gov/sra/?term="

for srr in sys.argv[1:]:
    srrMatch = SRRPattern.search(srr)
    if srrMatch:
        srrID = srrMatch.group(0)
        handle = urllib.urlopen(sraURL + srrID)
        text = handle.read()
        handle.close()

        srxMatch = SRXPattern.search(text)
        srsMatch = SRSPattern.search(text)
        if srxMatch and srsMatch:
            print srr + "\t" + srsMatch.group(0) + "\t" + srxMatch.group(0)
    time.sleep(1)
        
    
