#!/usr/bin/python
# trackDb: set up track hub files for methbase
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#                         Liz Ji <lizji1992@gmail.com>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import glob
import string
import re
import yaml
import os
import argparse

import MethBase
import MethBase_Utils

assemToOrg = {"hg18":"Human", "hg19":"Human", "mm9":"Mouse",  "mm10":"Mouse", "gorGor3":"Gorilla", \
              "panTro2":"Chimp", "tair10":"Arabidopsis", "canFam3":"Dog", "danRer7":"Zebrafish"}

defaultSamples = ["Hodges-Human-2011", "Molaro-Sperm-2011", "Lister-ESC-2009", \
                 "Stadler-Mouse-2011", "None", "Kaaij-Mouse-Intestinal-2013", \
                 "Hsieh-Arabidopsis-2009"]

#-------------- track sketch ---

dataTypes = ["PMD", "PMR", "HMR", "Meth", "Read", "Allelic", "AMR", "HyperMR"]
dataGroup = """view DataType d1PMD=partially_methylated_domain d2PMR=partially_methylated_region d3HMR=hypomethylated_regions d4Meth=methylation_level d5Read=coverage d6Allelic=allelic_score d7AMR=allele_specific_methylated_regions d8HyperMR=hyper_methylated_regions d9HydroxyMeth=hydroxy_methylation_level"""

## write different data types
dataTypeIDs = ["d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9"]
dataTypes = ["PMD", "PMR", "HMR", "Meth", "Read", "Allelic", "AMR", "HyperMR", "HydroxyMeth"]
dataSuffixs = [".pmd.bb", ".pmr.bb", ".hmr.bb", ".meth.bw", ".read.bw", \
                ".allelic.bw", ".amr.bb", ".hypermr.bb", ".hydroxy.bw"]
trackTypes = ["bigBed", "bigBed", "bigBed", "bigWig", "bigWig", "bigWig", \
                "bigBed", "bigBed", "bigWig"]
dataColors = ["64,185,60", "0,139,139", "56,113,193", "197,172,24", "0,0,0", \
                "120,47,154", "120,47,154", "209,36,47", "250,112,37"]
bigWigSettings = ["", "", "", "viewLimits 0:1\n\t\tyLineOnOff on", \
                    "viewLimits 0:15\n\t\tyLineOnOff on", \
                    "viewLimits 0:1\n\t\tyLineOnOff on", "", "", \
                    "viewLimits 0:1\n\t\tyLineOnOff on"]
dataVisibilities = ["hide", "hide", "dense", "full", "hide", \
                    "hide", "hide", "dense", "full"] 

projectSetting = {}
projectSetting['Pub'] = """track {project}
compositeTrack on
shortLabel {shortLabel}
longLabel {longLabel}
priority {priority}
subGroup1 {dataGroup}
subGroup2 {sampleGroup}
dimensions dimY=sample
dragAndDrop on
type bigBed
sortOrder sample=+ view=+
visibility {visi}
noInherit on
"""
projectSetting['CellType'] = """track {project}
compositeTrack on
shortLabel {shortLabel}
longLabel {longLabel}
priority {priority}
subGroup1 {dataGroup}
subGroup2 {sampleGroup}
subGroup3 {projectGroup}
dimensions dimY=sample
dragAndDrop on
type bigBed
sortOrder sample=+ project=+ view=+
visibility {visi}
noInherit on
"""
dataSetting = """\ttrack {track}
\tparent {parent}
\tshortLabel {shortLabel}
\tview {dataType}
\tvisibility {visibility}
\ttype {trackType}\n\n"""
trackSetting = {}
trackSetting['Pub'] = """\t\ttrack {track}
\t\tbigDataUrl {url}
\t\ttype {trackType}
\t\tshortLabel {shortLabel}
\t\tlongLabel {longLabel}
\t\tsubGroups view={dataType} sample={sample}
\t\twindowingFunction mean
\t\tpriority {priority}
\t\tmaxHeightPixels 32:24:24
\t\tcolor {color}
\t\tparent {parent} {parentOnOff}
\t\tvisibility {visibility}
\t\t{bigWigSetting}\n\n"""
trackSetting['CellType'] = """\t\ttrack {track}
\t\tbigDataUrl {url}
\t\ttype {trackType}
\t\tshortLabel {shortLabel}
\t\tlongLabel {longLabel}
\t\tsubGroups view={dataType} sample={sample} project={project}
\t\twindowingFunction mean
\t\tpriority {priority}
\t\tmaxHeightPixels 32:24:24
\t\tcolor {color}
\t\tparent {parent} {parentOnOff}
\t\tvisibility {visibility}
\t\t{bigWigSetting}\n\n"""



################################################################################

def rmpunc(s): return s.replace("_", "").replace("-", "").replace(".", "")

def loadAllProjects(dir, assembly):
    projects = []
    trackfiles = glob.glob(dir + "*/*/tracks_" + assembly + "/*.*.*b[bw]")
    trackfiles = [t.replace(dir, "", 1) for t in trackfiles]
    proj_list = list(set([t.split("/")[0] for t in trackfiles]))
    for proj_id in proj_list:
        proj_dir = os.path.join(dir, proj_id)
        projects.append(MethBase.Project(proj_dir))
    return projects

def getPubTrackFiles(projects, assembly):
    #-------------------------------------------
    def get_from_config(file, assembly):
        proj_trackfiles = []
        IN = open(file)
        x = yaml.load(IN)
        for s in x['Samples']:
            sampledir = x['Samples'][s]['Path']
            ignore_list = []
            if x['Samples'][s].has_key('Ignore') and \
                    x['Samples'][s]['Ignore'] != None:
                ignore_list = x['Samples'][s]['Ignore']
            found_trackfiles = glob.glob(os.path.join(sampledir, 'tracks_' + \
                    assembly, '*.*.*b[bw]'))
            for f in found_trackfiles:
                if f.split('.')[-2] not in ignore_list:
                    proj_trackfiles.append(f)

        IN.close()
        return proj_trackfiles

    def get_from_dir(projdir, assembly):
        proj_trackfiles = glob.glob(os.path.join(projdir, '*/tracks_' + \
                    assembly, '*.*.*b[bw]'))
        return proj_trackfiles
    #-------------------------------------------
    PubTrackFiles = {}
    for project in projects:
        config = os.path.join(project.Dir, 'project.config')
        if os.path.isfile(config) and MethBase_Utils.isYAMLConfig(config):
            PubTrackFiles[project.id] = get_from_config(config, assembly)
        else:
            PubTrackFiles[project.id] = get_from_dir(project.Dir, assembly)
    return PubTrackFiles

def getCTTrackFiles(wkdir, file_organization, assembly):
    celltypes = []
    trackfiles = []
    CTTrackFiles = {}
    IN = open(file_organization, 'r')
    struct_dirs = yaml.load(IN)
    IN.close()
    if not struct_dirs.has_key(assembly):
        return (celltypes, CTTrackFiles)
    for c in struct_dirs[assembly]:
        celltypes.append(MethBase.CellType(file_organization, wkdir, assembly, c))
        CTTrackFiles[c] = []
        for p in struct_dirs[assembly][c]:
            for s in struct_dirs[assembly][c][p]:
                trackdir = os.path.join(wkdir, p, s, 'tracks_'+assembly)
                CTTrackFiles[c] += glob.glob(trackdir + "/*.*.*b[bw]")
    return (celltypes, CTTrackFiles)

################################################################################

def sep_orig_lift(samples, assembly):
    ## this is to make sure those sample of the original species come first
    samplesOrig = []
    samplesLift = []
    for s in samples:
        if s.find(assemToOrg[assembly]) != -1:
            samplesOrig.append(s)
        else:
            samplesLift.append(s)
    return samplesOrig, samplesLift

def sortProjects(projects, PubTrackFiles, assembly):
    # non-liftover projects come first
    projectsOrig = []
    projectsLift = []
    for p in projects:
        samples = [t.split("/")[-3] for t in PubTrackFiles[p.id] \
                   if t.find(p.id) != -1]
        (samplesOrig, samplesLift) = sep_orig_lift(samples, assembly)
        if len(samplesOrig) > 0:
            projectsOrig.append(p)
        else:
            projectsLift.append(p)
    projectsOrig.sort(key=lambda x: x.shortLabel)
    projectsLift.sort(key=lambda x: x.shortLabel)
    projects = projectsOrig + projectsLift
    return projects


def format_shortLabel(project, samplesOrig, species, type_label):
    species_label = '' if len(samplesOrig) > 0 else '{%s}' % (species)
    #fullname = project.shortLabel if project.shortLabel != None else project.id
    #prior_labels = re.findall('(\[\w+\])', fullname)
    #realname = fullname.split('] ')[1] if fullname[0] == '[' else fullname
    shortLabel = type_label + species_label + ' ' + project.shortLabel
    return shortLabel

def isSampleLevel(samplename):
    flag = True
    items = samplename.split('_')
    pat = re.compile('R\d+')
    if pat.search(items[-1]):
        flag = False
    return flag

def writeTrackDb(TRACKDBOUT, project, project_tracks, \
                  priority, assembly, wkdir, url_prefix, type):
    
    if type == 'CellType':
        samples = project.samples.keys()
    else:
        samples = [t.split('/')[-3] for t in project_tracks]
    ## this is to make sure those sample of the original species come first
    (samplesOrig, samplesLift) = sep_orig_lift(samples, assembly)
    samples = samplesOrig + samplesLift
    samples = list(set(samples))
    ## deal with liftoverred project
    species = ''
    if len(samplesOrig) == 0 and len(samplesLift) > 0:
        species = samplesLift[0].split('_', 1)[0].split('-', 1)[0]
            
    if type == 'CellType':
        projectGroup = 'project Publication ' \
                + ' '.join([p.replace('_', '').replace('-', '') + '=' + p \
                for p in project.projects.keys()])
        sampleGroup = 'sample Celltype ' \
                + " ".join([s.replace("_", "").replace("-", "") + "=" + \
                s.split(':')[1].replace("-", "_") for s in samples])
    else:
        sampleGroup = 'sample Celltype ' \
                + " ".join([s.replace("_", "").replace("-", "") + "=" + \
                s.replace("-", "_") for s in samples])

    write_projectSetting = ''
    if type == 'CellType':
        write_projectSetting = projectSetting[type].format(project = project.id.replace("-", "_"), \
                        shortLabel = format_shortLabel(project, samplesOrig, species, ''), \
                        priority = str(priority), \
                        longLabel = project.longLabel if project.longLabel != None else project.id, \
                        dataGroup = dataGroup, \
                        sampleGroup = sampleGroup, \
                        projectGroup = projectGroup, \
                        visi = "full" if len(samplesOrig) > 0 and project.id in defaultSamples else "hide")

    else:
        write_projectSetting = projectSetting[type].format(project = project.id.replace("-", "_"), \
                        shortLabel = format_shortLabel(project, samplesOrig, species, '[Pub]'), \
                        priority = str(priority), \
                        longLabel = project.longLabel if project.longLabel != None else project.id, \
                        dataGroup = dataGroup, \
                        sampleGroup = sampleGroup, \
                        visi = "full" if len(samplesOrig) > 0 and project.id in defaultSamples else "hide")
    TRACKDBOUT.write(write_projectSetting)
    if assembly == "tair10":
        TRACKDBOUT.write("group meth\n\n")
    else:
        TRACKDBOUT.write("\n")
                                
    for i in range(len(dataTypes)):
        tracks = [t for t in project_tracks if t.find(dataSuffixs[i]) != -1]
        if len(tracks) == 0: continue

        write_dataSetting = ''
        write_dataSetting = dataSetting.format(\
                      track = dataTypes[i] + project.id.replace("-","_"), \
                      parent = project.id.replace("-", "_"), \
                      shortLabel = dataTypes[i], \
                      dataType = dataTypeIDs[i] + dataTypes[i], \
                      visibility = dataVisibilities[i], \
                      trackType = trackTypes[i])

        TRACKDBOUT.write(write_dataSetting)

        for t in tracks:
            trackfile = t.split("/")[-1]
            write_trackSetting = ''
            if type == 'CellType':
                samplename = t.split("/")[-3]
                projname = t.split('/')[4]
                sample = projname + ':' + samplename
                sampleID = samples.index(sample)
                write_trackSetting = trackSetting[type].format(\
                            track = "CellType_" + rmpunc(projname) + "_" \
                                    + rmpunc(trackfile.replace(dataSuffixs[i], "_"+dataTypes[i])), \
                            url = t.replace(wkdir, url_prefix), \
                            trackType = trackTypes[i], \
                            shortLabel = samplename.replace(assemToOrg[assembly] + "_", ""), \
                            longLabel = trackfile.replace(dataSuffixs[i], "_"+dataTypes[i]) \
                                        if dataTypes[i] != 'Meth' else \
                                        project.projects[projname].longLabel + ' : ' + \
                                        trackfile.replace(dataSuffixs[i], '_'+dataTypes[i]), \
                            dataType = dataTypeIDs[i] + dataTypes[i], \
                            sample = sample.replace("-", "").replace("_", ""), \
                            project = projname.replace("-", "").replace("_", ""), \
                            priority = str(priority + sampleID*0.01 + i*0.0001), \
                            color = dataColors[i], \
                            parent = dataTypes[i] + project.id.replace("-","_"), \
                            parentOnOff = "" if ((len(samplesOrig) > 0 and sample in samplesOrig) or \
                                                len(samplesOrig) == 0) and sampleID < 6  else "off", \
                            visibility = dataVisibilities[i] if ((len(samplesOrig) > 0 and sample in samplesOrig) or \
                                            len(samplesOrig) == 0) and sampleID < 6  else "hide", \
                            bigWigSetting = bigWigSettings[i])
            else:
                sample = t.split("/")[-3]
                sampleID = samples.index(sample)
                write_trackSetting = trackSetting[type].format(\
                            track = rmpunc(project.id) + "_" \
                                    + rmpunc(trackfile.replace(dataSuffixs[i], "_"+dataTypes[i])), \
                            url = t.replace(wkdir, url_prefix), \
                            trackType = trackTypes[i], \
                            shortLabel = sample.replace(assemToOrg[assembly] + "_", ""), \
                            longLabel = trackfile.replace(dataSuffixs[i], "_"+dataTypes[i]), \
                            dataType = dataTypeIDs[i] + dataTypes[i], \
                            sample = sample.replace("-", "").replace("_", ""), \
                            priority = str(priority + sampleID*0.01 + i*0.0001), \
                            color = dataColors[i], \
                            parent = dataTypes[i] + project.id.replace("-","_"), \
                            parentOnOff = "" if ((len(samplesOrig) > 0 and sample in samplesOrig) or \
                                                len(samplesOrig) == 0) and sampleID < 6  else "off", \
                            visibility = dataVisibilities[i] if ((len(samplesOrig) > 0 and sample in samplesOrig) or \
                                            len(samplesOrig) == 0) and sampleID < 6 and isSampleLevel(sample) else "hide", \
                            bigWigSetting = bigWigSettings[i])

            TRACKDBOUT.write(write_trackSetting)
          
def main():

    parser = argparse.ArgumentParser(description='configure MethBase tracks.')
    parser.add_argument('-a', '--assembly', dest = 'assembly', required = True,
                        help = 'assembly id, e.g., hg18')
    parser.add_argument('-t', '--track', dest = 'trackfile', required = True,
                        help='track configuration file')
    parser.add_argument('-f', '--file', dest = 'file_organization',
                        default = '/labdata/methylome/public/Cell_Type/organization.txt', 
                        help = 'organization file to construct cell type tracks')
    parser.add_argument('-d', '--dir', dest = 'dir', 
                        default = "/labdata/methylome/public/", 
                        help='top level directory containing projects')
    parser.add_argument('-u', '--url', dest = 'url', 
                        default = "http://smithlab.usc.edu/methbase/data/",
                        help='prefix of track file URL')
    
    args = parser.parse_args()
    if args.dir[-1] != '/': args.dir += '/'
    if args.url[-1] != '/': args.url += '/'

    assembly = args.assembly
    file_organization = args.file_organization
    
    ############################################################################
    
    TRACKDBOUT = open(args.trackfile, "w")
    
    # write cell type tracks first
    # get cell type files
    (celltypes, CTTrackFiles) = getCTTrackFiles(args.dir, file_organization, assembly)
    priority = 1
    celltypes.sort(key=lambda x: x.shortLabel)
    for celltype in celltypes:
        celltype_tracks = CTTrackFiles[celltype.id]
        writeTrackDb(TRACKDBOUT, celltype, celltype_tracks, priority, \
                assembly, args.dir, args.url, 'CellType')
        priority += 1

    # load all projects
    projects = loadAllProjects(args.dir, assembly)
    # get track files
    PubTrackFiles = getPubTrackFiles(projects, assembly)
    # sort projects: projects of the original species come first
    projects = sortProjects(projects, PubTrackFiles, assembly)
    

    # write project tracks
    for project in projects:
        project_tracks = PubTrackFiles[project.id]
        if len(project_tracks) == 0: continue
        writeTrackDb(TRACKDBOUT, project, project_tracks, priority, \
                assembly, args.dir, args.url, 'Pub')
        priority += 1
    
    TRACKDBOUT.close()    
        
if __name__ == '__main__':
    main()

