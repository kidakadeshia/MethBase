# MethBase.py : interface to represent a BS-seq project, including
#               its samples and metadata
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

import os
import glob
import yaml
from MethBase_Utils import MyConfigParser
from MethBase_Utils import isYAMLConfig
from MethBase_Utils import tab2spaces

class Sample:
    def default_contructor(self):
        self.ID = None
        self.Organism = None
        self.Description = None
        self.Strategy = None
        self.authors = None
        self.SRAID = None
        self.PMID = None
        self.smithlabDir = None
        self.SEPE = None
        self.kit = None
        self.readLength = None
        self.bsRate = None
        self.meanCoverage = None
        self.numCpGs = None
        self.coveredCpGs = None
        self.numCs = None
        self.coveredCs = None
        self.meanMethC = None
        self.meanMethCpG = None
        self.numHMR = None
        self.HMRMedianSize = None
        self.numAMR = None
        self.numPMD = None
        self.numHyperMRCpG = None
        self.numHyperMRAll = None
        self.Dir = None
        self.ProjDir = None
        self.URL = None
        self.Ignore = []

    def getMetaDataFromStr(self, str):
        fields = str.split(",")
        self.Organism = fields[1]
        self.Description = fields[2]
        self.authors = fields[3].split("; ")
        self.SRAID = fields[4]
        self.PMID = fields[5]
        self.smithlabDir = fields[6]
        self.SEPE = fields[7]
        self.kit = fields[8]
        self.readLength = fields[9]
        self.bsRate = fields[10]
        self.meanCoverage = fields[11]
        self.coveredCpGs = fields[12]
        self.coveredCs = fields[13]
        self.meanMethC = fields[14]
        self.meanMethCpG = fields[15]
        self.numHMR = fields[16]
        self.HMRMedianSize = fields[17]
        self.numAMR = fields[18]
        self.numPMD = fields[19]
        
    def __init__(self, str = None):
        if str == None:
            self.default_contructor()
        else:
            self.getMetaDataFromStr(str)
        
    def setMethStats(self, methstatfile, allOrCpG = "CpG"):
        if os.path.isfile(methstatfile):
            f = open(methstatfile)
            if allOrCpG == "CpG":
                self.numCpGs = int(f.readline().split(":")[-1])
                self.coveredCpGs = int(f.readline().split(":")[-1])
                f.readline()
                f.readline()
                self.meanCoverage = float(f.readline().split(":")[-1])
                f.readline()
                self.meanMethCpG = float(f.readline().split(":")[-1])
            else:
                self.numCs = int(f.readline().split(":")[-1])
                self.coveredCs = int(f.readline().split(":")[-1])
                f.readline()
                f.readline()
                self.meanCoverage_all = float(f.readline().split(":")[-1])
                f.readline()
                self.meanMethC = float(f.readline().split(":")[-1])

    def setLevelStats(self, levelsfile):
        if os.path.isfile(levelsfile):
            f = open(levelsfile)
            firstline = f.readline()
            if firstline.find('NUMBER OF CHROMOSOMES') != -1: # new .levels file
                f.close()
                tab2spaces(levelsfile)
                f = open(levelsfile)
                x = yaml.load(f)
                self.numCpGs = int(x['SYMMETRICAL CpG MEAN COVERAGE']) 
                self.numCs = int(x['SITES'])
                self.coveredCpGs = int(x['SYMMETRICAL CpG SITES COVERED'])
                self.coveredCs = int(x['SITES COVERED'])
                self.meanCoverage = float(x['SYMMETRICAL CpG MEAN COVERAGE'])
                self.meanCoverage_all = float(x['MEAN COVERAGE']) 
                self.meanMethCpG = float(x['METHYLATION LEVELS (CpG CONTEXT)'].split()[3])
                self.meanMethC = float(x['METHYLATION LEVELS (CpG CONTEXT)'].split()[3])
            else:
                f.seek(0, 0)
                f.readline()
                self.meanMethCpG = float(f.readline().split()[1])
                f.readline()
                line = f.readline()
                fields = line.split()
                self.numCpGs = int(fields[4].strip(")"))
                self.coveredCpGs = int(round(float(fields[1]) * self.numCpGs))
                
    def setBsrate(self, fn):
        if os.path.isfile(fn):
            f = open(fn)
            line = f.readline()
            if line.find("=") != -1:
                self.bsRate = float(line.split("=")[-1])
            
    def setHMRStats(self, fn):
        if 'hmr' not in self.Ignore and os.path.isfile(fn):
            i = 0
            for l in open(fn):
                i += 1
            self.numHMR = i

    def setAMRStats(self, fn):
        if 'amr' not in self.Ignore and os.path.isfile(fn):
            i = 0
            for l in open(fn):
                i += 1
            self.numAMR = i

    def setPMDStats(self, fn):
        if 'pmd' not in self.Ignore and os.path.isfile(fn):
            i = 0
            for l in open(fn):
                i += 1
            self.numPMD = i

    def setHyperMRStats(self, fn, allOrCpG = "CpG"):
        if 'hypermr' not in self.Ignore and os.path.isfile(fn):
            i = 0
            for l in open(fn):
                i += 1

            if allOrCpG == "CpG":
                self.numHyperMRCpG = i
            else:
                self.numHyperMRAll = i
            
    def setDir(self, dirn):
        if os.path.exists(dirn):
            self.Dir = os.path.abspath(dirn)
            self.URL = self.Dir.replace("/labdata/methylome/public/", "http://smithlab.usc.edu/methbase/data/")
            lvs = self.Dir.split('/')
            self.ProjDir = os.path.join('/'+lvs[0], lvs[1], lvs[2], lvs[3], lvs[4])            

    def setMeta(self, fn):
        if not os.path.isfile(fn): return
        f = open(fn)
        line = f.readline()
        f.close()
        if len(line.split(",")) == 20:
            self.getMetaDataFromStr(line)
        else:
            metafile = MyConfigParser(fn)
            for k, v in metafile.items:
                setattr(self, k, v)

    def setConfigMeta(self, fn):
        if not os.path.isfile(fn): return
        if isYAMLConfig(fn):
            FIN = open(fn, 'r')
            config = yaml.load(FIN)
            for field in config['Samples'][self.ID]:
                setattr(self, field, config['Samples'][self.ID][field])
            self.ProjConf = True
        else:
            metafile = MyConfigParser(fn)
            for k, v in metafile.items:
                setattr(self, k, v)
            self.ProjConf = False
                
    def getMetaDataFromDir(self, sample_dir):
        self.Ignore = []
        self.setDir(os.path.abspath(sample_dir))
        self.ID = os.path.basename(os.path.dirname(self.Dir))
        self.setMeta(os.path.join(self.Dir, self.ID + ".meta"))
        self.setConfigMeta(os.path.join(self.ProjDir, "project.config"))
        if self.Ignore == None:
            self.Ignore = []
        self.setMethStats(os.path.join(self.Dir, self.ID + ".methstats"))
        self.setMethStats(os.path.join(self.Dir, self.ID + ".all.methstats"),"All")
        self.setLevelStats(os.path.join(self.Dir, self.ID + ".levels"))
        self.setBsrate(os.path.join(self.Dir, self.ID + ".bsrate"))
        self.setHMRStats(os.path.join(self.Dir, self.ID + ".hmr"))
        self.setAMRStats(os.path.join(self.Dir, self.ID + ".amr"))
        self.setPMDStats(os.path.join(self.Dir, self.ID + ".pmd"))
        self.setHyperMRStats(os.path.join(self.Dir, self.ID + ".hypermr"))
        self.setHyperMRStats(os.path.join(self.Dir, self.ID + ".all.hypermr"),"All")

    def is_WGBS(self):
        if self.ProjConf:
            if self.Strategy and self.Strategy.find("WGBS") != -1:
                return True
            else:
                return False
        else:
            if self.Strategy and self.Strategy.find("RRBS") != -1:
                return False
            else:
                return True
        
    def toHtmlTableRow(self):
        line = "<tr>"
        line += '<td><input type="checkbox" name="samples" value="' \
          + self.ID + '" checked>' + self.ID + '</td>'
        line += "<td>{0}</td>".format(self.Organism) \
            + "<td>{0}</td>".format(self.Description) \
            + "<td>{0}</td>".format(self.authors[0] + " et al") \
            + "<td>{0}</td>".format(self.SRAID) \
            + "<td>{0}</td>".format(self.PMID) \
            + "<td>{0}</td>".format('<a href="show-page.py?sampleID=' \
                                    + self.ID + '"> More info </a>')
        line = line + "</tr>"
        return line



class Project:
    def __init__(self, project_dir):
        self.shortLabel = None
        self.longLabel = None
        self.PMID = None
        project_dir = os.path.abspath(project_dir)
        self.Dir = project_dir
        self.id = os.path.basename(project_dir)

        project_metafile = os.path.join(project_dir, self.id + ".meta")
        if os.path.isfile(project_metafile):
            metadata = MyConfigParser(project_metafile)
            for k, v in metadata.items:
                setattr(self, k, v)

        project_config = os.path.join(project_dir, "project.config")
        if os.path.isfile(project_config):
            if isYAMLConfig(project_config):
                FIN = open(project_config, 'r')
                config = yaml.load(FIN)
                for field in config:
                    if field != 'Samples':
                        setattr(self, field, config[field])
            else: 
                metadata = MyConfigParser(project_config)
                for k, v in metadata.items:
                    setattr(self, k, v)

        if self.shortLabel == None:
            self.shortLabel = self.id
        self.shortLabel = self.shortLabel.split(']')[-1].strip()
        if self.longLabel == None:
            self.longLabel = self.id
        

class CellType:
    def __init__(self, fn, dir, assembly, celltype):
        self.id = celltype
        self.shortLabel = celltype
        self.longLabel = celltype
        self.projects = {}
        self.samples = {}

        IN = open(fn)
        x = yaml.load(IN)
        for p in x[assembly][celltype]:
            projdir = os.path.join(dir, p)
            self.projects[p] = Project(projdir)
            for s in x[assembly][celltype][p]:
                sampledir = os.path.join(dir, p, s, 'results_' + assembly)
                sample = Sample()
                sample.getMetaDataFromDir(sampledir)
                self.samples[p+':'+s] = sample

class MethBase:
    def __init__(self, methbase_dir):
        self.dir = os.path.abspath(methbase_dir)
        trackfiles = glob.glob(self.dir + "*/*/tracks_*/*.meth.bw")
        projdirs = [os.path.dirname(os.path.dirname(os.path.dirname(f))) \
                    for f in trackfiles]
        self.projdirs = list(set(projdirs))

    def summary(self):
        assemToOrg = {"hg19":"Human", "mm9":"Mouse", \
                      "panTro2":"Chimp", "tair10":"Arabidopsis"}

        s = "Species\tStudies\tSamples\n"
        for k in assemToOrg.iterkeys():
            trackfiles = glob.glob(self.dir + "/*/*/tracks_" + k + "/" + \
                                   assemToOrg[k] + "*.meth.bw")
            projdirs = [os.path.dirname(os.path.dirname(os.path.dirname(f))) \
                        for f in trackfiles]
            projdirs = list(set(projdirs))
            s += "{Species}\t{Studies:d}\t{Samples:d}\n".format( \
                Species = assemToOrg[k], \
                Studies = len(projdirs), \
                Samples = len(trackfiles))
        return s

    def summaryTable(self):
        assemToOrg = {"hg19":"Human", "mm9":"Mouse", \
                      "panTro2":"Chimp", "tair10":"Arabidopsis"}

        table = """
<table>
<tr><td>Species</td><td>Studies</td><td>Samples</td></tr>
"""
        for k in assemToOrg.iterkeys():
            trackfiles = glob.glob(self.dir + "/*/*/tracks_" + k + "/" + \
                                   assemToOrg[k] + "*.meth.bw")
            projdirs = [os.path.dirname(os.path.dirname(os.path.dirname(f))) \
                        for f in trackfiles]
            projdirs = list(set(projdirs))
            table += "<tr><td>{Species}</td><td>{Studies:d}</td><td>{Samples:d}</td></tr>\n".format( \
                Species = assemToOrg[k], \
                Studies = len(projdirs), \
                Samples = len(trackfiles))
        table += "</table>"
        return table

class CheckMethBase:
    def __init__(self):
        self.RootDir = '/labdata/methylome/public/'
        self.CheckItems = {}
        self.CheckItems['Mammal'] = ['meth', 'hmr', 'allelic', 'amr', 'pmr', 'bsrate', 'methstats']
        self.CheckItems['Arabidopsis'] = ['meth', 'hypermr', 'allelic', 'amr', 'pmr', 'bsrate', 'methstats', 'all.meth', 'all.methstats']

def load_samples():
    samples = list()
    metaDataFile = "methpipe-database-metadata.csv"
    f = open(metaDataFile)
    line = f.readline()
    for line in f:
        s = Sample(line)
        if s != None:
            samples.append(s)
    return samples
        
def select_samples(samples, ID):
    for i in samples:
        if i.ID == ID:
            return i
        
    return None

def print_header():
    print "Content-type: text/html\n\n"
    print "<html>"
    print "<head>"
    print "<title>List of Methylomes: </title>"
    print "</head>"
    print "<body>"
    print "<h1>MethBase: gateway to hundreds of reference methylomes</h1>"

def print_footer():
    print "</body>"
    print "</html>"

if __name__ == '__main__':
    print "Running MethBase module"



