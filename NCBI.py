#NCBI.py : Provide an object-oriented interface to NCBI PubMed records
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

from Bio import Entrez
Entrez.email = "qiang.song@usc.edu"
import unicodedata

import xml.etree.ElementTree as XMLTree

class PubMedArticle:
    def __init__(self, ID):
        self.valid = False
        try:
            handle = Entrez.efetch(db = "pubmed", id = ID, retmode = "xml")
        except:
            return
        text = handle.read()
        handle.close()
        xmltree = XMLTree.fromstring(text)
        article = xmltree.find("PubmedArticle").find("MedlineCitation").find("Article")
        idlist = xmltree.find("PubmedArticle").find("PubmedData").find("ArticleIdList")
        self.valid = False
        if article != None:
            self.title = article.find("ArticleTitle").text
            journal = article.find("Journal")
            self.journal = journal.find("ISOAbbreviation").text
            
            journalIssue = journal.find("JournalIssue")
            self.year = journalIssue.find("PubDate").find("Year").text
            self.volume = journalIssue.find("Volume").text if not journalIssue.find("Volume") is None else ""
            self.issue = journalIssue.find("Issue").text if not journalIssue.find("Issue") is None else ""
            for articleid in idlist:
                if articleid.get("IdType") == "doi": 
                    self.doi = articleid.text
            self.page = article.find("Pagination").find("MedlinePgn").text
            self.authors = list()
            for author in article.find("AuthorList").findall("Author"):
                lastname = author.findtext("LastName") if not author.findtext("LastName") is None else ""
                initials = author.findtext("Initials") if not author.findtext("Initials") is None else ""
                self.authors.append(lastname + " " + initials)
            self.valid = self.title != None \
              and self.journal != None \
              and self.year != None \
              and self.volume != None \
              and self.issue != None \
              and self.doi != None \
              and self.page != None \
              and self.authors != None
            
    def getAuthorList(self):
        if self.valid:
            return self.authors
        else:
            return list()
        
    def getMLAHtml(self):
        if self.valid:
            authors = ", ".join(self.authors) if len(self.authors) <= 10 else ", ".join(self.authors[0:10]) + ", et al"
            title = """<a href="http://dx.doi.org/{doi}">{title}</a>"""
            title = title.format(doi = self.doi, title = self.title)
            text = authors + " " + title + " " + self.journal + ". " + self.year + " " + self.volume + "(" + self.issue + "):" + self.page  
            if isinstance(text, str):
                return text
            else:
                return unicodedata.normalize('NFKD', text).encode('ascii','ignore')
        else:
            return ""
        
if __name__ == '__main__':
    import sys
    import time
    for n in sys.argv[1:]:
        article = PubMedArticle(n)
        authors = article.getAuthorList()
        print n + "\t" + "; ".join(authors)
        time.sleep(1)
        
    
