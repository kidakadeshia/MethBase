# methbaseDesc.py : Generate an html file with an overall summary of MethBase
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#! /usr/bin/env python

import sys
import os
import glob
import re

from MethBase import Sample
from MethBase import MethBase
from NCBI import PubMedArticle
import MethBase
import argparse

def get_html_header():
    desc = """
<html>
<head>
<title>MethBase: the reference methylome database</title>
</head>
<body>
<h1>MethBase summary</h1>
"""
    return desc

def get_html_footer():
    return """

</body>
</html>
"""

def main():

    parser = argparse.ArgumentParser(description='genreate MethBase summary.')
    parser.add_argument('-o', '--output', dest = 'html_file', default = "",
                        help='html output file')
    parser.add_argument('-d', '--dir', dest = 'dir', required = True,
                        help='MethBase root directory')

    args = parser.parse_args()

    methbase = MethBase.MethBase(args.dir)
    
    html_file = args.html_file if args.html_file else "/dev/stdout"
    f = open(html_file, "w")
    f.write(get_html_header())
    f.write(methbase.summaryTable())
    f.write(get_html_footer())
    f.close()
    
if __name__ == '__main__':
    main()
