# list-samples.py
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

# Import modules for CGI handling 
import cgi
import cgitb 
cgitb.enable()

from MethBase import Sample, load_samples, print_header, print_footer

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

def select_samples(samples):
        
    # Get data from fields
    if form.getvalue('organism'):
        organism = form.getvalue('organism')
    else:
        organism = "all"

    selectedSampleIdx = list()
    for i in range(len(samples)):
        name = samples[i].organism.lower()
        if organism == "all":
            ctedSampleIdx.append(i)
        elif organism == "hg19":
            if "human" in name or "homo" in name or "sapiens" in name:
                selectedSampleIdx.append(i)
        elif organism == "mm9":
            if "mouse" in name or "mice" in name or "mus" in name:
                selectedSampleIdx.append(i)
        elif organism == "tair10":
            if "arabidopsis" in name or "thaliana" in name:
                selectedSampleIdx.append(i)

    return selectedSampleIdx


def writeSeletedSampleTable(samples, selectedSampleIdx):
    print "<table>"
    print """
<tr>
  <th>ID</th>
  <th>Organism</th>
  <th>Sample</th>
  <th>Authors</th>
  <th>SRA </th>
  <th>PudMed </th>
  <th>More info </th>
</tr>
  """
    for i in selectedSampleIdx:
        print samples[i].toHtmlTableRow()
    print "</table>"

def print_search_option():
    print """
<form name="form" action="list-samples.py" method="post">
<input type="submit" value="New search" />
Organism <select name="organism"> 
  <option value="hg19" > Human </option>
  <option value="mm9" > Mouse </option>
  <option value="tair10" > Arabidopsis </option>
  <option value="all" > All </option>
</select>

Cell type: <input type="text" name="cellType">

Sample condition: <select name="state"> 
  <option value="normal" > normal sample </option>
  <option value="cancer" > cancerous sample </option>
  <option value="celline" > cultured cell lines </option>
  <option value="all" > all conditions </option>
</select>

</form>
    """
    
def print_next_action():
    print """
<input type="submit" name = "nextAction" value="Show in Genome Browser" />
<input type="submit" name = "nextAction" value="Download data" />
<input type="checkbox" name="tracks" value="methLevel" checked> Methylation level
<input type="checkbox" name="tracks" value="coverage">Coverage
<input type="checkbox" name="tracks" value="HMR" checked> HMRs
<input type="checkbox" name="tracks" value="AMR"> AMRs
<input type="checkbox" name="tracks" value="methcountFile">Methcount files
    """

if __name__ == '__main__':

    samples = load_samples()
    selectedSampleIdx = select_samples(samples)
    
    print_header()
    print "<h2> New search: </h2>"
    print_search_option()
    print """<form action="next-action.py" method="post" target = "_blank">"""
    print "<h2> Next action: </h2>"
    print_next_action()
    print "<h2>" + str(len(selectedSampleIdx)) +  " samples found </h2>"
    writeSeletedSampleTable(samples, selectedSampleIdx)
    print """</form>"""
    print_footer()
