# ValiMethBase.py: Identifies incomplete projects or incorrect directory
#                  structures. Future option for individual project checks.
#
# Copyright (C) 2013-2017 University of Southern California and Liz Ji
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

import os
import sys
import re
import argparse
import yaml
import pwd

from MethBase import CheckMethBase


#============================================
def isValidProject(projectname):
    # If the directory name contains numbers(year)
    # then it's regarded as a valid project 
    if re.findall('\d+', projectname):
        return True
    else:
        return False


def isValidSampleReplicate(path):
    # Valid sample or replicate must contains results or tracks
    for item in os.listdir(path):
        if not os.path.isdir(path+'/'+item):
            continue
        if re.findall('results', item):
            return True
        elif re.findall('tracks', item):
            return True
    return False

#=============================================

class DataIntergrity:
    def setUser(self):
        checkfile = os.path.join(self.path, self.results[0], \
                                self.name+'.meth')
        if os.path.isfile(checkfile):
            stat = os.lstat(checkfile)
            pw = pwd.getpwuid(stat.st_uid)
            self.user = pw.pw_name

    def parseConfig(self, config):
        if os.path.isfile(config):
            IN = open(config)
            header = IN.readline()
            if header.find('YAML') != -1:
                x = yaml.load(IN)
                if x.has_key('processedBy'):
                    self.user = x['processedBy']
                if x['Samples'].has_key(self.name):
                    if x['Samples'][self.name].has_key('Ignore'):
                        self.ignore = x['Samples'][self.name]['Ignore'] \
                                if x['Samples'][self.name]['Ignore'] != None else [] 

    def LocateDataDirs(self):               
        for item in os.listdir(self.path):
            if os.path.isdir(os.path.join(self.path, item)):
                if re.findall('results', item):
                    self.results.append(item)
                elif re.findall('tracks', item):
                    self.tracks.append(item)
        self.results.sort()
        self.tracks.sort()

    def CheckAllFolders(self, info, species):
        def CheckExistence(items, CompleteSet, IgnoreSet):
            # Check if the results are complete
            RequiredSet = list(set(CompleteSet)-set(IgnoreSet))
            missing = []
            for goal in RequiredSet:
                notfound = True
                for item in items:
                    if item.find('.'+goal) != -1:
                        notfound = False
                        break
                if notfound:
                    missing.append(goal)
            return missing

        #/////////////////////////
        for result in self.results:
            items = os.listdir(os.path.join(self.path, result))
            missing = CheckExistence(items, info.CheckItems[species], self.ignore)
            if missing != []:
                self.missing[result] = missing;

        for track in self.tracks:
            items = os.listdir(os.path.join(self.path, track))
            missing = CheckExistence(items, info.CheckItems[species], self.ignore)
            if missing != []:
                self.missing[track] = missing;

    def __init__(self, dir_work, info, species, config):
        self.path = dir_work
        self.name = os.path.basename(dir_work)
        self.user = 'NA'
        self.ignore = []
        self.results = []
        self.tracks = []
        self.missing = {}

        self.LocateDataDirs()
        self.setUser()
        self.parseConfig(config)
        self.CheckAllFolders(info, species)


#=============================================
class CheckProject:
    def getSpecies(self):
        if re.findall('Arabidopsis', self.name):
            self.species = 'Arabidopsis'
        else:
            self.species = 'Mammal' # default   


    def CheckSamples(self, info):
        config = os.path.join(self.path, 'project.config')
        for item in os.listdir(self.path):
            path_work = os.path.join(self.path, item)
            if os.path.isdir(path_work):
                if not re.findall('results', item) and \
                       not re.findall('tracks', item):
                    if isValidSampleReplicate(path_work):
                        self.samples.append(DataIntergrity(path_work, \
                                    info, self.species, config))

    def __init__(self, path_project, info):
        self.path = path_project 
        self.name = os.path.basename(path_project)
        self.species = 'NA'
        self.samples = []
        
        self.getSpecies()
        self.CheckSamples(info)

#=============================================
def main():
    
    #=============================================================
    parser = argparse.ArgumentParser(description = 'Validate MethBase files')
    parser.add_argument('-i', '--input', dest='rootdir', \
                        default='/labdata/methylome/public', \
                        help='where the database is (default: %(default))') 
    parser.add_argument('-o', '--output', dest='output', \
                        default='/labdata/methylome/public/ValidationReport.txt', \
                        help='the output file (default: %(default))')   
    parser.add_argument('-p', '--project', dest='project_dirs', nargs='+', \
                        help='only scan the projects given (dir names)')    
    parser.add_argument('-u', '--user', dest='users', nargs='+', \
                        help='only scan the projects established by the users given')   

    args = parser.parse_args()

    #=============================================================


    info = CheckMethBase()
    
    if not args.project_dirs:
        args.project_dirs = [i for i in os.listdir(args.rootdir) \
                             if os.path.isdir(args.rootdir+'/'+i)]

    OUT = open(args.output, 'w')
    print >>OUT, '\t'.join(['Project', 'Owner', 'User', 'Data', 'Missing'])
    
    for project_dir in args.project_dirs:
        if not isValidProject(project_dir):
            continue
        
        print 'Scanning %s...' % (project_dir)    

        writenew = False        
        project = CheckProject(os.path.join(args.rootdir, project_dir), info)
        for sample in project.samples:
            if args.users and sample.user not in args.users:
                continue
            for result in sample.results:
                if sample.missing.has_key(result):
                    print >>OUT, '\t'.join( [project.name, sample.name, \
                        sample.user, result, ','.join(sample.missing[result])] )
                    writenew = True
            for track in sample.tracks:
                if sample.missing.has_key(track):
                    print >>OUT, '\t'.join( [project.name, sample.name, sample.user, \
                        track, ','.join(sample.missing[track])] )
                    wrtenew = True
        if writenew:
            print >>OUT, ''


if __name__ == "__main__":
    main()
