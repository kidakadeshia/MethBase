#! /bin/bash
# update-trackhub.sh: completely update MethBase
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cd /labdata/methylome/public;

## write configuration file
echo setting up track hub configuration files
for assembly in hg18 hg19 panTro2 mm9 rheMac3 tair10;
do
    echo "updating trackhub for assembly: $assembly" 	
    /usr/local/bin/python ~qiangson/Documents/methbase/trackDb.py \
	--assembly $assembly \
	--track methbase/trackhub_test/$assembly/trackDb.txt \
	--dir $PWD \
	--url http://smithlab.usc.edu/methbase/data;    	
    echo "done";
    echo;
done

## trackhub validation
echo validating track hub configuration files
~qiangson/app/GenomeBrowserTool/hubCheck  http://smithlab.usc.edu/methbase/trackhub_test/hub.txt && echo All good
rm -rf /tmp/udcCache


