# show-page.py: Import modules for CGI handling
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

import cgi
import cgitb 
cgitb.enable()

from MethBase import Sample, load_samples, print_header, print_footer

        
def select_samples(samples, ID):
    for i in samples:
        if i.ID == ID:
            return i
        
    return None

def write_sample(s):
    print "<h2>" + s.ID +  "</h2>"
    print "<h3>Description</h3>"
    print "<table>"

    refurl = "https://www.ncbi.nlm.nih.gov/pubmed/" + s.PMID
    pubmedurl = '<a href="' + refurl + '">' + refurl + '</a>'

    refurl = "https://www.ncbi.nlm.nih.gov/sra/" + s.ID
    SRAurl = '<a href="' + refurl + '">' + refurl + '</a>'
    
    print "<tr><td>{0}</td><td>{1}</td></tr>".format("Organism", s.organism) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("Cell type", s.sample) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("Producer", "; ".join(s.authors)) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("Reference", pubmedurl) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("SRA source", SRAurl) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("Technique", s.kit+";"+s.SEPE+";")  

    print "</table>"

    print "<h3>Methylome summary</h3>"
    print "<table>"

    print "<tr><td>{0}</td><td>{1}</td></tr>".format("read length", str(s.readLength)) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("converion rate", str(s.bsRate)) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("mean coverage", str(s.meanCoverage)) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("covered  CpGs", str(s.coveredCpGs)) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("covered  Cs", str(s.coveredCs)) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("No. HMR", str(s.numHMR)) \
      + "<tr><td>{0}</td><td>{1}</td></tr>".format("No. AMR", str(s.numAMR))
    print "</table>"
    
def write_related_samples(samples, s):
    print "<h3> Related methylomes </h3>"
    for i in samples:
        if i.PMID == s.PMID:
            print '<a href="show-page.py?sampleID=' + i.ID + '">' \
              + i.ID + '</a>,&nbsp'



def write_methods():
    print "<h2>Methods</h2>"
    print """
<p>All analysis was done using a DNA methylation data analysis
pipeline, MethPipe.</p>

Mapping

RRBS reads were mapped to the reference genome, hg19, using
RMAP. Duplicates were not removed.

HMRs, Hypomethylated Regions

<p>The distribution of methylation levels at individual sites in a
methylome (either CpGs or non-CpG Cs) almost always has a bimodal
distribution with one peak low (very close to 0) and another peak high
(close to 1). In most mammalian cells, the majority of the genome has
high methylation, and regions of low methylation are typically more
interesting. These are called hypo-methylated regions. To identify the
HMRs, the hmr program was used. The hmr program uses a hidden Markov
model (HMM) approach using a Beta-Binomial distribution to describe
methylation levels at individual sites while accounting for the number
of reads informing those levels. hmr automatically learns the average
methylation levels inside and outside the HMRs, and also the average
size of those HMRs. The HMM parameters are trained on the data being
analyzed, since the parameters depend on the average methylation level
and variance of methylation level; the variance observed can also
depend on the coverage.</p>

CpG Methylation

<p>In computing single-site methylation levels, the methylation level
for every CpG site at single base resolution in an adult mammalian
cell is estimated as a probability based on the ratio of methylated to
unmethylated reads mapped to that loci. Since CpG methylation is
symmetric, reads mapped to both strands are used to produce a single
estimate for the CpG site. To compute the average methylation level in
a genomic interval, the sorted single-site methylation levels and a
sorted BED format file of genomic regions of interest are
scored. Average methylation levels are computed through the interval,
weighted according to the number of reads informing about each CpG or
C in the methylation file. If there are not reads mapping in a region,
then the methylation level is undefined and that region is not
output.</p>

CpG Coverage

<p>Coverage is determined by the number of reads at that site.</p>
    """


if __name__ == '__main__':

    samples = load_samples()
    
    print_header()

    # Create instance of FieldStorage 
    form = cgi.FieldStorage() 

    if "sampleID" in form:
        ID = form.getvalue('sampleID')
        s = select_samples(samples, ID)
        if s != None:
            write_sample(s)
            write_related_samples(samples, s)
            write_methods()
        else:
            print "<p> No Sample found with ID: " + str(ID) + "</p>"
    else:
            print "<p> No Sample ID given </p>"

    print_footer()


