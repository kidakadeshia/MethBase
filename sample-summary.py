# sample-summary.py: Generates HTML summary of a sample for trackhub.
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#! /usr/bin/env python

import sys
import os
import glob
import re

from MethBase import Sample
from NCBI import PubMedArticle
import MethBase
import argparse

def get_html_header(s):
    desc = """
<html>
<head>
<title>{ID}</title>
</head>
<body>

"""
    return desc.format(ID = s)
 
def get_sample_desc(s):
    desc = """
<h2>  {ID} </h2>
<h3>Description</h3>
<table>
<tr><td>Organism</td><td>{organism}</td></tr>
<tr><td>Sample</td><td>{sample}</td></tr>
<tr><td>Authors</td><td>{authors}</td></tr>
<tr><td>PubMed</td><td>{pubmed}</td></tr>
<tr><td>SRA</td><td>{SRA}</td></tr>
<tr><td>Technique</td><td>{technique}</td></tr>  
</table>

<h3>Methylome summary</h3>
<table>
<tr><td>read length</td><td>readLength</td></tr>
<tr><td>Conversion rate</td><td>{bsrate}</td></tr>
<tr><td>Methylation</td><td>{meanMethCpG}</td></tr>
<tr><td>Coverage</td><td>{meanCoverage}</td></tr>
<tr><td>Coverage CpGs</td><td>{coveredCpGs}</td></tr>
<tr><td>#HMR</td><td>{numHMR}</td></tr>
<tr><td>#AMR</td><td>{numAMR}</td></tr>
<tr><td>#PMD</td><td>{numPMD}</td></tr>
</table>

"""
    def get(v): return str(v) if v != None else ""
    return desc.format(ID = get(s.ID), \
                       organism = get(s.organism), \
                       sample = get(s.sample), \
                       authors = get(s.authors), \
                       pubmed = get(s.PMID), \
                       SRA = get(s.SRAID), \
                       technique = get(s.kit) + "; " + get(s.SEPE), \
                       readLength = get(s.readLength), \
                       bsrate = get(s.bsRate), \
                       meanMethCpG = get(s.meanMethCpG), \
                       meanCoverage = get(s.meanCoverage), \
                       coveredCpGs = get(s.coveredCpGs), \
                       numHMR = get(s.numHMR), \
                       numAMR = get(s.numAMR), \
                       numPMD = get(s.numPMD))


def toTableRow(s, assembly = ""):
    def get(v): return str(v) if v != None else ""
    def get_num(x): 
        try:
            return float(x)
        except:
            return 0

    if assembly == "tair10":
        text = """<tr>
<td>{ID}</td>
<td>{sample}</td>
<td>{bsrate:.3f}</td>
<td>{meanMethC:.3f}</td>
<td>{meanCoverage:.3f}</td>
<td>{coveredCpGs:.3f}</td>
<td>{coveredCs:.3f}</td>
<td>{numHyperMRCpG:.0f}</td>
<td>{numHyperMRAll:.0f}</td>
</tr>
"""
        return text.format(ID = get(s.ID), \
              sample = get(s.sample), \
              bsrate = get_num(s.bsRate), \
              meanMethC = get_num(s.meanMethC), \
              meanCoverage = get_num(s.meanCoverage), \
              coveredCpGs = get_num(s.coveredCpGs) / get_num(s.numCpGs), \
              coveredCs = get_num(s.coveredCs) / get_num(s.numCs), \
              numHyperMRCpG = get_num(s.numHyperMRCpG), \
              numHyperMRAll = get_num(s.numHyperMRAll))
    else:
        text = """<tr>
<td>{ID}</td>
<td>{sample}</td>
<td>{bsrate:.3f}</td>
<td>{meanMethCpG:.3f}</td>
<td>{meanCoverage:.3f}</td>
<td>{coveredCpGs:.3f}</td>
<td>{numHMR:.0f}</td>
<td>{numAMR:.0f}</td>
<td>{numPMD:.0f}</td>
<td>{Note}</td>
</tr>
"""
        notes = ""
        if s.is_RRBS():
            notes += """<span style="font-weight:bold">RRBS;&nbsp</span>"""
        if get_num(s.bsRate) > 0.0 and get_num(s.bsRate) < 0.95:
            notes += """<span style="color:red">LowBS;&nbsp</span>"""
        if get_num(s.meanCoverage) > 0.0 and get_num(s.coveredCpGs)  > 0 \
          and get_num(s.numCpGs) > 0.0: 
            meanCoverage = get_num(s.meanCoverage)
            if s.is_RRBS(): meanCoverage = meanCoverage*get_num(s.numCpGs)/get_num(s.coveredCpGs)
            if meanCoverage < 10.0:
                notes += """<span style="color:darkorange">LowCov;&nbsp</span>"""
        notes += """<a href="{url}">Download</a>""".format(url=s.URL.replace("results_", "tracks_"))
            
        return text.format(ID = get(s.ID), \
                sample = get(s.sample), \
                bsrate = get_num(s.bsRate), \
                meanMethCpG = get_num(s.meanMethCpG), \
                meanCoverage = get_num(s.meanCoverage), \
                coveredCpGs = get_num(s.coveredCpGs) / get_num(s.numCpGs) if get_num(s.numCpGs) > 0 else 0.0, \
                numHMR = get_num(s.numHMR), \
                numAMR = get_num(s.numAMR), \
                numPMD = get_num(s.numPMD), \
                Note = notes)

def main():

    parser = argparse.ArgumentParser(description='genreate project summary.')
    parser.add_argument('-a', '--assembly', dest = 'assembly', required = True,
                        help = 'assembly id, e.g., hg18')
    parser.add_argument('-o', '--output', dest = 'html_file', default = "",
                        help='html output file')
    parser.add_argument('-d', '--dir', dest = 'dir', required = True,
                        help='project directory')

    args = parser.parse_args()
    
    project_dir = os.path.abspath(args.dir)
    assembly = args.assembly

    project_id = os.path.basename(project_dir).replace("-", "_")
    sample_dirs = glob.glob(project_dir + "/*/results_" + assembly)
    sample_dirs = sorted(sample_dirs)
    if len(sample_dirs) == 0:
        sys.stderr.write("No results for assembly:" + assembly + "\n")
        sys.exit(-1)
    PMID = ""
    
    RRBS_flag = False
    Low_conversion_flag = False
    Low_coverage_flag = False
    for sample_dir in sample_dirs:
        s = Sample()
        s.getMetaDataFromDir(sample_dir)
        tablerow = toTableRow(s, assembly)
        print s.Dir, tablerow.replace("\n", "")
        
if __name__ == '__main__':
    main()
