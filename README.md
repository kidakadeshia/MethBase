MethBase Maintenance Tools
====

These software packages include scripts to maintain the MethBase
reference methylome (http://smithlabresearch.org/software/methbase/). 

To read intructions on how to maintain MethBase, please refer to

    http://smithlabresearch.org/other/how-to-update-methbase-with-new-methylomes

Note that you must be logged in to the website in ordere to view the page.

Contact: 
Andrew D Smith <andrewds@usc.edu>
Song Qiang <qiang.song@usc.edu>
