# ValiProjConfig.py: Check if the project.config file is good.
#
# Copyright (C) 2013-2017 University of Southern California and Liz Ji
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import argparse
import yaml

OFFSET = 4

def stop_err(message, exit_code=1):
    sys.stderr.write(message)
    sys.exit(exit_code)

def check_file_exist(fpath):
    if not os.path.isfile(fpath):
        stop_err('ERROR: project.config does not exist.')

def tab2spaces(fpath):
    # substitute spaces for tabs
    IN = open(fpath)
    tmpfile = fpath + '.tmp'
    TMPOUT = open(tmpfile, 'w')
    text = IN.read().replace('\t', ' '*OFFSET)
    TMPOUT.write(text)
    TMPOUT.close()
    os.remove(fpath)
    os.rename(tmpfile, fpath)

def check_header(lines):
    if lines[0].find('#YAML') != 0:
        stop_err('ERROR: the first line should be #YAML')

def check_fields(x, keywords):
    for field in keywords:
        if field not in x:
            stop_err('ERROR: %s field is required.' % (field))

def check_samples(samples):
    for s in samples:
        if not samples[s].has_key('Path'):
            stop_err('ERROR: the directory path of sample %s is not given.' % (s))
        else:
            p = samples[s]['Path']
            if not os.path.isdir(p):
                stop_err('ERROR: the directory path of sample %s is not correct' % (s))

#==============================================================================
def main():
    parser = argparse.ArgumentParser(description = 'Validate project \
             configuration file')
    parser.add_argument('-i', '--input', dest='indir', required=True, \
                        help='where the project directory is')
    args = parser.parse_args()
    #--------------------------------------------------------------------------

    fpath = os.path.join(args.indir, 'project.config')
    check_file_exist(fpath)
    tab2spaces(fpath)

    IN = open(fpath)
    lines = [i for i in IN.readlines() if i.rstrip() != '']
    IN.close()

    check_header(lines)
    IN = open(fpath)
    try:
        x = yaml.load(IN)
    except Exception as e:
        stop_err('ERROR: yaml parsing failed. Please check the format: \n' + str(e))

    project_fields = ['shortLabel', 'longLabel', 'PMID', 'Samples']
    check_fields(x, project_fields)
    check_samples(x['Samples'])
    print 'Project configuration is loaded successfully.'

if __name__ == "__main__":
  main()
